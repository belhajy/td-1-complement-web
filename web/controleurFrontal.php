<?php

////////////////////
// Initialisation //
////////////////////
use Symfony\Component\HttpFoundation\Request;
use TheFeed\Controleur\RouteurURL;

require_once __DIR__ . '/../vendor/autoload.php';


/////////////
// Routage //
/////////////
RouteurURL::traiterRequete(Request::createFromGlobals())->send();

