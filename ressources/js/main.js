/**
 * @param {HTMLElement} button La balise <button> cliquée
 */
function supprimerPublication(button) {
    // TODO : récupérer l'identifiant de publication de la balise button
    let idPublication = button.dataset.id;
    let URL = apiBase + "publications/" + idPublication;

    fetch(URL, {method: "DELETE"})
        .then(response => {
            if (response.status === 200) {
                // Plus proche ancêtre <div class="feedy">
                let divFeedy = button.closest("div.feedy");
                divFeedy.remove();
            }
        });
}

function templatePublication(publication, utilisateur) {
    return `<div class="feedy">
   <div class="feedy-header">
      <a href="${pagePersoBase + publication.auteur.idUtilisateur}">
            <img alt="profile picture" src="${imgBase}/utilisateurs/${utilisateur.nomPhotoDeProfil}" class="avatar">
      </a>
      <div class="feedy-info">
            <span>${utilisateur.login}</span><span> - </span><span>${publication.date}</span>
            <p>${publication.message}</p>
            <button class="delete-feedy" data-id-publication="${publication.idPublication}" onclick="supprimerPublication(this)">Supprimer</button>
      </div>
   </div>
</div>`;
}

async function soumettrePublication() {
    const messageElement = document.getElementById('message')
    // On récupère le message
    let message = messageElement.value;
    // On vide le formulaire
    messageElement.value = "";
    // On utilise la variable globale apiBase définie dans base.html.twig
    let URL = apiBase + "publications";

    let response = await fetch(URL, {
        // Ajouter la méthode 'POST'
        method : "POST",
        // Ajouter un corps de requête contenant le message
        body: JSON.stringify({message: message}),
        // Ajouter des en-têtes pour indiquer
        // * le format du corps de requête
        // * le format de données attendu en retour
        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json; charset=UTF-8',
        },
    });
    if (response.status !== 201)
        // (Hors TD) Il faudrait traiter l'erreur
        return;
    let publication = await response.json();
    // Utilisateur par défaut en attendant la suite
    let utilisateur = {nomPhotoDeProfil : "anonyme.jpg", login: "Inconnu"};
    let formElement = document.getElementById("feedy-new");
    formElement.insertAdjacentHTML('afterend', templatePublication(publication, utilisateur));
}

let boutonAjouter = document.getElementById("feedy-new-submit");
boutonAjouter.addEventListener("click", soumettrePublication);

let BoutonSupprimer = document.querySelectorAll(".delete-feedy");

BoutonSupprimer.forEach(button => {
    button.addEventListener("click", function(event) {
        supprimerPublication(event.target);
    });
});