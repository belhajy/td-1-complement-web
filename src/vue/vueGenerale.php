<?php

use TheFeed\Lib\ConnexionUtilisateur;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\Conteneur;

/**
 * @var string $pagetitle
 * @var string $cheminVueBody
 * @var String[][] $messagesFlash
 * @var UrlGenerator $generateurUrl
 * @var UrlHelper $assistantUrl
 */

$generateurUrl = Conteneur::recupererService("generateurUrl");
$assistantUrl = Conteneur::recupererService("assistantUrl");
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title><?= $pagetitle ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?=$assistantUrl->getAbsoluteUrl("../ressources/css/styles.css")?>">
</head>

<body>
    <header>
        <div id="titre" class="center">
            <a href="<?=$generateurUrl->generate("afficherListe")?>"><span>The Feed</span></a>
            <nav>
                <a href="<?=$generateurUrl->generate("afficherListe")?>">Accueil</a>
                <?php
                if (!ConnexionUtilisateur::estConnecte()) {
                ?>
                    <a href="<?=$generateurUrl->generate("afficherFormulaireCreation")?>">Inscription</a>
                    <a href="<?=$generateurUrl->generate("afficherFormulaireConnexion")?>">Connexion</a>
                <?php
                } else {
                    $idUtilisateurURL = rawurlencode(ConnexionUtilisateur::getIdUtilisateurConnecte());
                ?>
                    <a href="<?=$generateurUrl->generate("afficherPublications", ["idUtilisateur" => ConnexionUtilisateur::getIdUtilisateurConnecte()])?>">Ma
                        page</a>
                    <a href="<?=$generateurUrl->generate("deconnecter")?>">Déconnexion</a>
                <?php } ?>
            </nav>
        </div>
    </header>
    <div id="flashes-container">
        <?php
        foreach (["success", "error"] as $type) {
            foreach ($messagesFlash[$type] as $messageFlash) {
        ?>
                <span class="flashes flashes-<?= $type ?>"><?= $messageFlash ?></span>
        <?php
            }
        }
        ?>
    </div>
    <?php
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</body>

</html>