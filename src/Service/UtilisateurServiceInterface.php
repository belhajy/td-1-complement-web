<?php

namespace TheFeed\Service;

interface UtilisateurServiceInterface
{
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil);

    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true);

    public function connecter($login, $motDePasse);

    public function deconnecter();
}