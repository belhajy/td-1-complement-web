<?php

namespace TheFeed\Service;

use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements UtilisateurServiceInterface
{

    public function __construct(private UtilisateurRepositoryInterface $utilisateurRepository,
                                private string $dossierPhotoDeProfil,
                                private FileMovingServiceInterface $fileMovingService){
        
    }

    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil) {
        //TO-DO

        //Verifier que les attributs ne sont pas null
        if ($login != null && $motDePasse != null && $email != null && $donneesPhotoDeProfil != null) {
            //Verifier la taille du login
            if (strlen($login) < 4 || strlen($login) > 20) {
                throw new ServiceException("Le login doit être compris entre 4 et 20 caractères!");
            }
            //Verifier la validité du mot de passe
            if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
                throw new ServiceException("Mot de passe invalide!");
            }
            //Verifier le format de l'adresse mail
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new ServiceException("L'adresse mail est incorrecte!");
            }
            //Verifier que l'utilisateur n'existe pas déjà
            $utilisateurRepository = $this->utilisateurRepository;
            $utilisateur = $utilisateurRepository->recupererParLogin($login);
            if ($utilisateur != null) {
                throw new ServiceException("Ce login est déjà pris!");
            }
            //Verifier que l'adresse mail n'est pas prise
            $utilisateur = $utilisateurRepository->recupererParEmail($email);
            if ($utilisateur != null) {
                throw new ServiceException("Un compte est déjà enregistré avec cette adresse mail!");
            }
            //Verifier extension photo de profil
            // Upload des photos de profil
            // Plus d'informations :
            // http://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/tut4-complement.html

            // On récupère l'extension du fichier
            $explosion = explode('.', $donneesPhotoDeProfil['name']);
            $fileExtension = end($explosion);
            if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
                throw new ServiceException("La photo de profil n'est pas au bon format!");
            }
            //Enregistrer la photo de profil
            // La photo de profil sera enregistrée avec un nom de fichier aléatoire
            $pictureName = uniqid() . '.' . $fileExtension;
            $from = $donneesPhotoDeProfil['tmp_name'];
            $to = $this->dossierPhotoDeProfil ."\\$pictureName";
            $this->fileMovingService->moveFile($from, $to);

            //Chiffrer le mot de passe
            $mdpHache = MotDePasse::hacher($motDePasse);

            //Enregistrer l'utilisateur...
            $utilisateur = Utilisateur::create($login, $mdpHache, $email, $pictureName);
            $utilisateurRepository->ajouter($utilisateur);
        } else {
            throw new ServiceException("Un ou plusieurs attributs sont null");
        }
    }

    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true) {
        $utilisateur = $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur);
        if(!$autoriserNull && $utilisateur == null) {
            throw new ServiceException("l’utilisateur n’existe pas!", Response::HTTP_NOT_FOUND);
        }
        return $utilisateur;
    }

    public function connecter($login, $motDePasse) {
        if(!(isset($login) && isset($motDePasse))) {
            throw new ServiceException("Login ou mot de passe manquant.");
        } else {
            $utilisateurRepository = $this->utilisateurRepository;
            $utilisateur = $utilisateurRepository->recupererParLogin($login);
            if($utilisateur == null) {
                throw new ServiceException("Login inconnu.");
            }
            if(!MotDePasse::verifier($motDePasse, $utilisateur->getMdpHache())) {
                throw new ServiceException("Mot de passe incorrect.");
            }
            ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
        }
    }

    public function deconnecter() {
        if (!ConnexionUtilisateur::estConnecte()) {
            throw new ServiceException("Utilisateur non connecté.");
        }
        ConnexionUtilisateur::deconnecter();
    }
}