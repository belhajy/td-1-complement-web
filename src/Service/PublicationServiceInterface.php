<?php

namespace TheFeed\Service;

use TheFeed\Modele\DataObject\Publication;

interface PublicationServiceInterface
{
    public function recupererPublications(): array;

    public function creerPublication($idUtilisateur, $message): Publication;

    public function recupererPublicationsUtilisateur($idUtilisateur): array;

    public function supprimerPublication(int $idPublication, ?string $idUtilisateurConnecte);

    public function recupererPublicationParId($idPublication, $autoriserNull = true): ?Publication;
}