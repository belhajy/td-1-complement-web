<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\MessageFlash;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurService;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Service\UtilisateurServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurUtilisateur extends ControleurGenerique {
    
    public function __construct(ContainerInterface $container ,private UtilisateurServiceInterface $utilisateurService, private PublicationServiceInterface $publicationService) {
        parent::__construct($container);
    }

    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name:'afficherPublications', methods:["GET"])]
    public function afficherPublications($idUtilisateur): Response
    {
        try {
            $utilisateur = $this->utilisateurService->recupererUtilisateurParId($idUtilisateur);
            if ($utilisateur === null) {
                MessageFlash::ajouter("error", "Login inconnu.");
                //return $this->rediriger("afficherListe");
            }
            $loginHTML = htmlspecialchars($utilisateur->getLogin());
            $publications = $this->publicationService->recupererPublicationsUtilisateur($idUtilisateur);
            return $this->afficherTwig('publication/page_perso.html.twig',[
                "publications" => $publications,
                'loginHTML' => $loginHTML
            ]);
        } catch (ServiceException $e) {
            return $this->afficherErreur($e->getMessage());
        }
    }
    #[Route(path: '/inscription', name:'afficherFormulaireCreation', methods:["GET"])]
    public function afficherFormulaireCreation(): Response
    {
        return $this->afficherTwig(
            "utilisateur/inscription.html.twig",
        );
    }

    #[Route(path: '/inscription', name:'creerDepuisFormulaireF', methods:["POST"])]
    public function creerDepuisFormulaire(): Response
    {
        $login = $_POST['login'];
        $motDePasse = $_POST['mot-de-passe'];
        $adresseMail = $_POST['email'];
        $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'];
        try {
            $this->utilisateurService->creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil);
            MessageFlash::ajouter("success", "Utilisateur créé avec succès.");
        } catch (ServiceException $e) {
            $aa = $e->getMessage();
            MessageFlash::ajouter("error", $e->getMessage());
        }
        return $this->rediriger("afficherFormulaireConnexion");
    }

    #[Route(path: '/connexion', name:'afficherFormulaireConnexion', methods:["GET"])]
    public function afficherFormulaireConnexion(): Response
    {
        return $this->afficherTwig('utilisateur/connexion.html.twig');
    }

    #[Route(path: '/connexion', name:'connecter', methods:["POST"])]
    public function connecter(): Response {
        try {
            $this->utilisateurService->connecter($_POST['login'], $_POST['mot-de-passe']);
            MessageFlash::ajouter("success", "Connexion effectuée.");
            return $this->rediriger("afficherListe");
        } catch (ServiceException $e) {
            return $this->afficherErreur($e->getMessage());
        }
    }

    #[Route(path: '/deconnexion', name:'deconnecter', methods:["GET"])]
    public function deconnecter(): Response
    {
        try {
            $this->utilisateurService->deconnecter();
            MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
            return $this->rediriger("afficherListe");
        } catch (ServiceException $e) {
            return $this->afficherErreur($e->getMessage());
        }
    }
}
