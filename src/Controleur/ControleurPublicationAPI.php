<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Exception\JsonException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\Exception\ServiceException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ControleurPublicationAPI extends ControleurGenerique
{
    public function __construct (
        ContainerInterface $container,
        private readonly PublicationServiceInterface $publicationService
    )
    {
        parent::__construct($container);
    }

    #[Route(path: '/api/publications/{idPublication}', name:'supprimer_publication', methods: ['DELETE'])]
    public function supprimer($idPublication): Response
    {
        try {
            $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
            $this->publicationService->supprimerPublication($idPublication, $idUtilisateurConnecte);
            return new JsonResponse('La publication à bien été supprimé', Response::HTTP_OK);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }

    #[Route(path: '/api/publications/{idPublication}', name:'afficher_detail_publication', methods: ['GET'])]
    public function afficherDetail($idPublication): Response {
        try {
            $publication = $this->publicationService->recupererPublicationParId($idPublication, false);
            return new JsonResponse($publication, Response::HTTP_OK);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }

    #[Route(path: '/api/publications', name:'afficher_liste_publications', methods: ['GET'])]
    public function afficherListe() {
        try {
            $publication = $this->publicationService->recupererPublications();
            return new JsonResponse($publication, Response::HTTP_OK);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }

    #[Route(path: '/api/publications', name:'poster_publication', methods: ['POST'])]
    public function posterPublication(Request $request): Response
    {
        try {
            // TODO : récupérer le message inclus dans la requête dans une variable $message
            $content = json_decode($request->getContent(), flags: JSON_THROW_ON_ERROR);
            $message = $content->message ?? null;
            $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
            $publication = $this->publicationService->creerPublication($idUtilisateurConnecte, $message);
            return new JsonResponse($publication, Response::HTTP_CREATED);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        } catch (JsonException $exception) {
            return new JsonResponse(
                ["error" => "Corps de la requête mal formé"],
                Response::HTTP_BAD_REQUEST
            );
        }
    }
}