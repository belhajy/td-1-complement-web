<?php

namespace TheFeed\Controleur;


use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurPublication extends ControleurGenerique {

    
    public function __construct(ContainerInterface $container ,private PublicationServiceInterface $publicationService) {
        parent::__construct($container);
    }

    #[Route(path: '/', name:'afficherListe', methods: ["GET"])]
    #[Route(path: '/publications', name:'afficherListe', methods:["GET"])]
    public function afficherListe() : Response
    {
        $publications = $this->publicationService->recupererPublications();
        return $this->afficherTwig('publication/feed.html.twig',[
            "publications" => $publications
        ]);
    }

    #[Route(path: '/publications', name:'creerDepuisFormulaire', methods:["POST"])]
    public function creerDepuisFormulaire() : Response
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try {
            //Utilisation du service
            $this->publicationService->creerPublication($idUtilisateurConnecte, $message);
            MessageFlash::ajouter("success", "Publication créée avec succès.");
        }
        catch(ServiceException $e) {
            //Ajout du message flash
            MessageFlash::ajouter("error", $e->getMessage());
        }

        return $this->rediriger('afficherListe');
    }


}