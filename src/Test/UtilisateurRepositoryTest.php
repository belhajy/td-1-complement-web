<?php
namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\ConnexionBaseDeDonneesInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;

class UtilisateurRepositoryTest extends TestCase
{
    private static UtilisateurRepositoryInterface  $utilisateurRepository;

    private static ConnexionBaseDeDonneesInterface $connexionBaseDeDonnees;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$connexionBaseDeDonnees = new ConnexionBaseDeDonnees(new ConfigurationBDDTestUnitaire());
        self::$utilisateurRepository = new UtilisateurRepository(self::$connexionBaseDeDonnees);
    }

    protected function setUp(): void
    {
        parent::setUp();
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (1, 'test', 'test', 'test@example.com', 'test.png')");
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (2, 'test2', 'test2', 'test2@example.com', 'test2.png')");
    }

    public function testSimpleNombreUtilisateurs() {
        $this->assertCount(2, self::$utilisateurRepository->recuperer());
    }

    public function testRecupererUtilisateurParId() {
        $utilisateur = self::$utilisateurRepository->recupererParClePrimaire(1);
        $this->assertEquals('test', $utilisateur->getLogin());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        self::$connexionBaseDeDonnees->getPdo()->query("DELETE FROM utilisateurs");
    }

}