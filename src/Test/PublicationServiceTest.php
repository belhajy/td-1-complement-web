<?php

namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;

class PublicationServiceTest extends TestCase{

    private $service;

    private $publicationRepositoryMock;

    private $utilisateurRepositoryMock;

    protected function setUp(): void {
        parent::setUp();
        $this->publicationRepositoryMock = $this->createMock(PublicationRepositoryInterface::class);
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->service = new PublicationService($this->publicationRepositoryMock, $this->utilisateurRepositoryMock);
    }

    public function testCreerPublicationUtilisateurInexistant() {
        $this->utilisateurRepositoryMock->method('recupererParClePrimaire')->with(-1)->willReturn(null);

        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Il faut être connecté pour publier un feed");

        $this->service->creerPublication(-1, "Test");
    }

    public function testCreerPublicationVide () {
        $fakeUser = new Utilisateur();
        $fakeUser->setIdUtilisateur(20);
        $this->utilisateurRepositoryMock->method('recupererParClePrimaire')->with(20)->willReturn($fakeUser);

        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Le message ne peut pas être vide!");

        $this->service->creerPublication(20, "");
    }

    public function testCreerPublicationTropGrande() {
        $fakeUser = new Utilisateur();
        $fakeUser->setIdUtilisateur(30);
        $this->utilisateurRepositoryMock->method('recupererParClePrimaire')->with(30)->willReturn($fakeUser);

        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Le message ne peut pas dépasser 250 caractères!");
        $this->service->creerPublication(30, str_repeat("a", 251));
    }

    public function testNombrePublications() {
        //Fausses publications, vides
        $fakePublications = [new Publication(), new Publication()];
        //On configure notre faux repository pour qu'il renvoie nos publications définies ci-dessus
        $this->publicationRepositoryMock->method("recuperer")->willReturn($fakePublications);
        //Test
        $this->assertCount(2, $this->service->recupererPublications());
    }

    public function testNombrePublicationsUtilisateur() {
        $fakeUser = new Utilisateur();
        $fakeUser->setIdUtilisateur(40);

        $fakePublications = [new Publication(), new Publication(), new Publication(), new Publication(), new Publication()];
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->with(40)->willReturn($fakeUser);
        $this->publicationRepositoryMock->method("recupererParAuteur")->with(40)->willReturn($fakePublications);
        $this->assertCount(5, $this->service->recupererPublicationsUtilisateur(40));
    }

    public function testNombrePublicationsUtilisateurInexistant() {
        $this->utilisateurRepositoryMock->method('recupererParClePrimaire')->with(50)->willReturn(null);
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("L'utilisateur n'existe pas!");
        $this->service->recupererPublicationsUtilisateur(50);
    }

    public function testCreerPublicationValide() {
        $fakeUser = new Utilisateur();
        $fakeUser->setIdUtilisateur(70);

        $this->utilisateurRepositoryMock->method('recupererParClePrimaire')->with(70)->willReturn($fakeUser);

        $this->publicationRepositoryMock->expects($this->once())
            ->method('ajouter')
            ->with($this->callback(function(Publication $publication) use ($fakeUser) {
                return $publication->getAuteur() === $fakeUser && $publication->getMessage() === "Test";
            }));

        $this->service->creerPublication(70, "Test");
    }
}