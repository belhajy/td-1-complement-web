<?php
namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\FileMovingServiceInterface;
use TheFeed\Service\UtilisateurService;

class UtilisateurServiceTest extends TestCase
{

    private $service;

    private $utilisateurRepositoryMock;

    //Dossier où seront déplacés les fichiers pendant les tests
    private  $dossierPhotoDeProfil = __DIR__."\\tmp";

    private FileMovingServiceInterface $fileMovingService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->fileMovingService = new TestFileMovingService();
        mkdir($this->dossierPhotoDeProfil);
        $this->service = new UtilisateurService($this->utilisateurRepositoryMock, $this->dossierPhotoDeProfil, $this->fileMovingService);
    }

    public function testCreerUtilisateurPhotoDeProfil() {
        $donneesPhotoDeProfil = [];
        $donneesPhotoDeProfil["name"] = "test.png";
        $donneesPhotoDeProfil["tmp_name"] = "test.png";
        $this->utilisateurRepositoryMock->method("recupererParLogin")->willReturn(null);
        $this->utilisateurRepositoryMock->method("recupererParEmail")->willReturn(null);
        $this->utilisateurRepositoryMock->method("ajouter")->willReturnCallback(function ($utilisateur) {
            $photo = $this->dossierPhotoDeProfil . "\\" . $utilisateur->getNomPhotoDeProfil();
            $this->assertTrue(file_exists($photo), "Profile photo does not exist!");
        });
        $this->service->creerUtilisateur("test", "TestMdp123", "test@example.com", $donneesPhotoDeProfil);
    }

    public function testCreerUtilisateurLoginCourt() {
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Le login doit être compris entre 4 et 20 caractères!");
        $this->service->creerUtilisateur("lol", "null", "null","null");
    }

    public function testCreerUtilisateurMdpIncorrect() {
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Mot de passe invalide!");
        $this->service->creerUtilisateur("belhajy", "null", "null","null");
    }

    public function testCreerUtilisateurMailIncorrect() {
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("L'adresse mail est incorrecte!");
        $this->service->creerUtilisateur("belhajy", "Yaniss.10", "null","null");
    }

    public function testCreerUtilisateurNull() {
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Un ou plusieurs attributs sont null");
        $this->service->creerUtilisateur(null, null, null,null);
    }

    public function testrecupererUtilisateurParId() {
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturnCallback(function ($id) {
            $utilisateur = new Utilisateur();
            $utilisateur->setIdUtilisateur($id);
            return $utilisateur;
        });
        $utilisateur = $this->service->recupererUtilisateurParId(1);
        $this->assertEquals(1, $utilisateur->getIdUtilisateur());
    }

    public function testrecupererUtilisateurParIdErreurLogin() {
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("l’utilisateur n’existe pas!");
        $this->service->recupererUtilisateurParId(70, false);
    }

    public function testSeConnecter() {
        $login="capom";
        $mdp="capom";
        $utilisateur = Utilisateur::create($login, MotDePasse::hacher($mdp), " ", " ");
        $this->utilisateurRepositoryMock->method("recupererParLogin")->willReturn($utilisateur);
        $this->service->connecter($login, $mdp);
        $this->assertTrue(ConnexionUtilisateur::estConnecte());
    }

    public function testSeConnecterErreurLoginOuMdpNull() {
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Login ou mot de passe manquant.");
        $this->service->connecter(null, "mauvaisMdp");
    }

    public function testSeConnecterErreurLogininexistant() {
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Login inconnu.");
        $this->service->connecter(60, "mauvaisMdp");
    }

    public function testSeConnecterErreurMdpIncorrect() {
        $login="capom";
        $mdp="capom";
        $utilisateur = Utilisateur::create($login, MotDePasse::hacher($mdp), " ", " ");
        $this->utilisateurRepositoryMock->method("recupererParLogin")->willReturn($utilisateur);
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Mot de passe incorrect.");
        $this->service->connecter($login, "mauvaisMdp");
    }

    public function testSeDeconnecter() {
        $login="capom";
        $mdp="capom";
        $utilisateur = Utilisateur::create($login, MotDePasse::hacher($mdp), " ", " ");
        $this->utilisateurRepositoryMock->method("recupererParLogin")->willReturn($utilisateur);
        $this->service->connecter($login, $mdp);
        $this->assertTrue(ConnexionUtilisateur::estConnecte());
        $this->service->deconnecter();
        $this->assertFalse(ConnexionUtilisateur::estConnecte());
    }

    public function testSedecconecterErreur() {
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Utilisateur non connecté.");
        $this->service->deconnecter();
    }

    protected function tearDown(): void
    {
        //Nettoyage
        parent::tearDown();
        foreach(scandir($this->dossierPhotoDeProfil) as $file) {
            if ('.' === $file || '..' === $file) continue;
            unlink($this->dossierPhotoDeProfil.$file);
        }
        rmdir($this->dossierPhotoDeProfil);
    }

}